﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Person[] people = new Person[]
            {
                new Person("James", 20, true),
                new Person("David", 25, true),
                new Person("Mary", 20, false),
                new Person("James", 25, true),
                new Person("John", 25, true),
                new Person("Linda", 20, false),
                new Person("Susan", 25, false),
                new Person("David", 25, true)
            };

            // 4. Using custom extensions
            Console.WriteLine("Initial array:");
            people.DescribeAll(" ");

            // 1. Select
            Console.WriteLine("\nSelecting Name:");
            var resultSelect = people.Select(p => p.Name);
            foreach (var el in resultSelect)
            {
                Console.WriteLine(" " + el);
            }

            // 2. Where
            Console.WriteLine("\nWhere age = 25:");
            var resultWhere = people.Where(p => p.Age == 25);
            foreach (var p in resultWhere)
            {
                Console.WriteLine(" " + p.AboutMe());
            }

            // 3. Dictionary
            Dictionary<string, Person> family = new Dictionary<string, Person>();
            family.Add("mother", new Person("Susan", 26, false));
            family.Add("father", new Person("Kevin", 32, true));
            family.Add("son", new Person("Bob", 5, true));

            Console.WriteLine("\nDictionary");
            Console.WriteLine(" [Key]    [Value]");
            foreach (var pair in family)
            {
                Console.WriteLine(" {0,-6} : {1}", pair.Key, pair.Value.AboutMe());
            }

            // 5. Anonymous classes
            var anon = new { name = "Petro", age = 30 };
            Console.WriteLine("\nThis is " + anon.name + " and he is " + anon.age);

            var anonResult = people.Where(p => p.IsMale == true).Select(p => new{name = p.Name, age = p.Age });
            Console.WriteLine("\nHere are all males in anonymous list:");
            foreach (var an in anonResult)
            {
                Console.WriteLine(" {0,-7} {1}", an.name, an.age);
            }

            // 6.
            Person[] peopleSort = (Person[])people.Clone();
            Console.WriteLine("\nPeople sorted by age");
            Array.Sort(peopleSort, new PersonAgeComparer());
            peopleSort.DescribeAll(" ");

            // 7.
            List<Person> peopleList = new List<Person>(people);
            peopleList.ToArray();

            // 8.
            Array.Sort(peopleSort.Select(p => p.Name).ToArray(), peopleSort);
            Console.WriteLine("\nArray sorted by name:");
            peopleSort.DescribeAll(" ");

            Console.ReadKey();
        }


    }
}
