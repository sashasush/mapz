﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    public interface IDescribable
    {
        string AboutMe();
    }
    public class Person : IDescribable
    {

        private static int _defaultAge = 20;
        private static string _defaultName = "Anonym";
        private static bool _defaultIsMale = true;

        private string name;
        private int age;
        private bool isMale;

        public string Name { get { return this.name; } set { this.name = value; } }
        public int Age { get { return this.age; } set { this.age = value > 1 ? value : 1; } }
        public bool IsMale { get { return this.isMale; } set { this.isMale = value; } }

        public Person() : this(_defaultName) { }
        public Person(string name) : this(name, _defaultAge) { }
        public Person(string name, int age) : this(name, age, _defaultIsMale) { }
        public Person(string name, bool isMale) : this(name, _defaultAge, isMale) { }
        public Person(string name, int age, bool isMale)
        {
            Name = name;
            Age = age;
            IsMale = isMale;
        }

        public string AboutMe()
        {
            return String.Format("{0,-7} {1,-6} {2,-2} yrs old", Name, (IsMale ? "male" : "female"), Age);
        }
    }

    public class PersonAgeComparer: IComparer<Person>
    {
        int IComparer<Person>.Compare(Person x, Person y)
        {
            return x.Age - y.Age;
        }
    }
}
