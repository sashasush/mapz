﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    public static class MyExtensions
    {
        public static void DescribeAll(
            this IEnumerable<IDescribable> list,
            string prefix = "",
            string postfix = "")
        {
            foreach (var el in list)
            {
                Console.WriteLine(prefix + el.AboutMe() + postfix);
            }
        }
    }

}
