﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1_10_11
{
    internal class Program
    {
        class Number
        {
            private int value;
            public int Value
            {
                get { return this.value; }
                set
                {
                    if (value < 0) { this.value = 0; return; }
                    if (value > 9) { this.value = 9; return; }
                    this.value = value;
                }
            }
            public Number()
            {
                this.Value = 0;
            }
            public Number(int number)
            {
                this.Value = number;
            }

            public static implicit operator int(Number n) { return n.Value; }
            public static explicit operator Number(int val) { return new Number(val); }
        }

        static void Main(string[] args)
        {
            { 
                /* boxing/unboxing */
                int value = 5;
                object objValue = value; // boxing
                int value2 = (int)objValue; // unboxing
            }
            {
                /* implicit and explicit */
                Number num1 = new Number(3);
                int a = num1;

                int b = 5;
                Number num2 = (Number)b;
            }
        }
    }
}
