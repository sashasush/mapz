﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1_5
{
    internal class Program
    {
        [Flags]
        enum TextFormat: byte
        {
            None = 0,
            Bold = 1 << 0,
            Itallic = 1 << 1,
            Underlined = 1 << 2,
            Crossed = 1 << 3,
            Monospaced = 1 << 4
        }

        static void PrintTextFormat(TextFormat format)
        {
            Console.Write("This text is ");
            if(TextFormat.None != (format & TextFormat.Bold))
            {
                Console.Write("bold ");
            }
            if(TextFormat.None != (format & TextFormat.Itallic))
            {
                Console.Write("itallic ");
            }
            if(TextFormat.None != (format & TextFormat.Underlined))
            {
                Console.Write("underlined ");
            }
            if(TextFormat.None != (format & TextFormat.Crossed))
            {
                Console.Write("crossed ");
            }
            if(TextFormat.None != (format & TextFormat.Monospaced))
            {
                Console.Write("monospace ");
            }
            Console.WriteLine();
        }
        static void Main(string[] args)
        {
            TextFormat textFormat = TextFormat.Bold | TextFormat.Underlined | TextFormat.Itallic;

            PrintTextFormat(textFormat);

            Console.ReadKey();
        }
    }
}
