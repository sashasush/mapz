﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1_6
{
    abstract class Food
    {
        public abstract string EatMe();
    }
    class Apple: Food
    {
        public override string EatMe()
        {
            return "apple";
        }
    }
    interface IPrintable
    {
        void Print();
    }
    interface IEatable
    {
        void Eat(Food food);
    }
    class Person: IPrintable, IEatable
    {
        private string name;
        private string surname;
        public Person():this("Steve", "Jobs") { }
        public Person(string name, string surname)
        {
            this.name = name;
            this.surname = surname;
        }
        public void Print()
        {
            Console.WriteLine("Hello, my name is " + name + " " + surname);
        }

        public void Eat(Food food)
        {
            Console.WriteLine(name + " " + surname + " eat " + food.EatMe());
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Person steve = new Person();
            Apple redApple = new Apple();

            steve.Print();
            steve.Eat(redApple);

            Console.ReadKey();
        }
    }
}
