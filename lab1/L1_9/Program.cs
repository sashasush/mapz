﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1_9
{
    internal class Program
    {
        static void Sum(ref int a, ref int b, out int result)
        {
            result = a + b;
        }
        static void Main(string[] args)
        {
            int val1 = 4, val2 = 5, result;
            Sum(ref val1, ref val2, out result);
            Console.WriteLine(result);
            Console.ReadKey();
        }
    }
}
