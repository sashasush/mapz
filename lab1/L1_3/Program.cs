﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1_3
{
    internal class Program
    {
        interface IMyInterface
        {
            void SomeMethod();
        }
        class MyRealization : IMyInterface
        {
            void IMyInterface.SomeMethod() { }
        }
        
        class MyClass
        {
            int value = 1;
        }

        class MyClassChild : MyClass
        {
            void Foo()
            {
                value = 1; 
            }
        }

        struct MyStruct
        {
            int value;
        }
        static void Main(string[] args)
        {
            MyClass myClass = new MyClass();
            myClass.value = 1;

            MyStruct myStruct = new MyStruct();
            myStruct.value = 1;
        }
    }
}
