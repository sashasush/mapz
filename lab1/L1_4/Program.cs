﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1_4
{
    internal class Program
    {
        class MyClass
        {
            protected class IntClass
            {
                public int value = 1;
            }
            protected IntClass intClass = new IntClass();

            public void Foo()
            {
                IntClass tmp = new IntClass();
            }
        }
        static void Main(string[] args)
        {
            MyClass myClass = new MyClass();
            IntClass intClass = new IntClass();
            myClass.Foo();
        }
    }
}
