﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1_2
{
    class A
    {
        public int      publicA;
        protected int   protectedA;
        private int     privateA;
        public void Foo()
        {
            int
                a = publicA,
                b = protectedA,
                c = privateA;
        }
    }

    class B : A
    {
        public int publicB;
        protected int protectedB;
        private int privateB;
        public void Bar()
        {
            int
                a = publicA,
                b = protectedA,
                c = privateA,

                d = publicB,
                e = protectedB,
                f = privateB;
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            A a = new A();

            a.publicA = 0;
            a.protectedA = 0;
            a.privateA = 0;
        }
    }
}
