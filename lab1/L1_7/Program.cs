﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1_7
{
    class Fruit
    {
        protected string name = "";
        public Fruit():this("banana") { }
        public Fruit(string name)
        {
            this.name = name;
        }
    }
    class Apple: Fruit
    {
        protected string color = "";
        public Apple() : base("apple")
        {
            this.color = "red";
        }
        public Apple(string color) : this()
        {
            this.color = color;
        }

        public void AboutMe()
        {
            Console.WriteLine("This " + name + " is " + color);
        }
        public void AboutMe(string beauty)
        {
            Console.WriteLine(beauty + " This " + name + " is " + color + " " + beauty);
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            Apple myApple = new Apple("green");
            myApple.AboutMe("**");

            Console.ReadKey();
        }
    }
}
