﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

// Варіант 7

namespace L1_12_1
{
    class Point
    {
        protected double x;
        protected double y;
        public Point()
        {
            x = 0.0;
            y = 0.0;
        }
        public Point(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
    }

    class Point3D : Point
    {
        protected double z;

        public Point3D() { }
        public Point3D(double x, double y, double z) : base(x, y)
        {
            this.z = z;
        }
    }
    
    struct SPoint
    {
        double x;
        double y;
        public SPoint(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
    }
    internal class Program
    {
        const uint arraySize = 20000000;
        const uint testCount = 5;
        static double TestClasses()
        {
            Stopwatch stopwatch = new Stopwatch();
            Point[] array = new Point[arraySize];

            Console.Write("Creating " + arraySize + " classes     | ");

            stopwatch.Start();
            for(int i = 0; i < array.Length; ++i)
            {
                array[i] = new Point(5.0, 5.0);
            }
            stopwatch.Stop();

            Console.WriteLine("Ended in " + String.Format("{0:00}.{1:000}", 
                stopwatch.Elapsed.Seconds,
                stopwatch.Elapsed.Milliseconds) + " s");

            return (int)stopwatch.Elapsed.TotalMilliseconds * 0.001;
        }
        
        static double TestInheritedClasses()
        {
            Stopwatch stopwatch = new Stopwatch();
            Point3D[] array = new Point3D[arraySize];

            Console.Write("Creating " + arraySize + " inh classes | ");

            stopwatch.Start();
            for(int i = 0; i < array.Length; ++i)
            {
                array[i] = new Point3D(5.0, 5.0, 5.0);
            }
            stopwatch.Stop();

            Console.WriteLine("Ended in " + String.Format("{0:00}.{1:000}", 
                stopwatch.Elapsed.Seconds,
                stopwatch.Elapsed.Milliseconds) + " s");

            return (int)stopwatch.Elapsed.TotalMilliseconds * 0.001;
        }
        
        static double TestStructures()
        {
            Stopwatch stopwatch = new Stopwatch();
            SPoint[] array = new SPoint[arraySize];

            Console.Write("Creating " + arraySize + " structures  | ");

            stopwatch.Start();
            for(int i = 0; i < array.Length; ++i)
            {
                array[i] = new SPoint(5.0, 5.0);
            }
            stopwatch.Stop();

            Console.WriteLine("Ended in " + String.Format("{0:00}.{1:000}", 
                stopwatch.Elapsed.Seconds,
                stopwatch.Elapsed.Milliseconds) + " s");

            return (int)stopwatch.Elapsed.TotalMilliseconds * 0.001;
        }

        static void Main(string[] args)
        {
            double mid;
            
            mid = 0.0;
            for(int i = 0; i < testCount; ++i)
            {
                mid += TestStructures();
            }
            mid /= testCount;
            Console.WriteLine("Middle time = " + mid + " s\n");

            mid = 0.0;
            for (int i = 0; i < testCount; ++i)
            {
                mid += TestClasses();
            }
            mid /= testCount;
            Console.WriteLine("Middle time = " + mid + " s\n");

            mid = 0.0;
            for (int i = 0; i < testCount; ++i)
            {
                mid += TestInheritedClasses();
            }
            mid /= testCount;
            Console.WriteLine("Middle time = " + mid + " s\n");

            Console.ReadKey();
        }
    }
}
