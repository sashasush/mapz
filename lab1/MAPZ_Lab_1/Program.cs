﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace L1_1
{
    interface ICar
    {
        void Start();
    }

    abstract class Truck : ICar
    {
        public virtual void Start()
        {
            Console.WriteLine("Truck started");
        }
        public abstract void Load();
    }

    class VolvoTruck : Truck
    {
        public override void Start()
        {
            Console.WriteLine("Volvo started");
        }

        public override void Load()
        {
            Console.WriteLine("Volvo Truck loaded");
        }

    }
    internal class Program
    {
        static void Main(string[] args)
        {
            VolvoTruck volvo = new VolvoTruck();
            ICar icVolvo = volvo as ICar;
            Truck trckVolvo = volvo as Truck;

            icVolvo?.Start();
            Console.WriteLine();

            trckVolvo?.Start();
            trckVolvo?.Load();
            Console.WriteLine();

            volvo.Start();
            volvo.Load();
            Console.WriteLine();

            Console.ReadKey();
        }
    }
}
